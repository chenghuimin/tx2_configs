0. Install GStreamer through apt first
1. Clone `https://github.com/TheImagingSource/tiscamera`
2. Change CMakeList.txt `BUILD_TOOLS` to `ON`
3. (Jetson TX2) Install dependencies `gstreamer-1.0 libusb-1.0 libglib2.0 libgirepository1.0-dev libudev-dev libtinyxml-dev libzip-dev libnotify-dev`
4. Install python dependencies `python3-gi python3-pyqt5`
5. use CMake build and install
